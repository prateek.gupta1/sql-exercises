---1. How can you produce a list of the start times for bookings by members named 'David Farrell'?

select cd.bookings.starttime 
from cd.members join cd.bookings on cd.members.memid = cd.bookings.memid where firstname = 'David' and surname = 'Farrell';

---2. How can you produce a list of the start times for bookings for tennis courts, for the date '2012-09-21'? Return a list of start time and facility name pairings, ordered by the time.

select cd.bookings.starttime , cd.facilities.name 
from cd.bookings join cd.facilities on cd.bookings.facid = cd.facilities.facid 
where cd.bookings.starttime > '2012-09-21' and cd.bookings.starttime < '2012-09-22' and cd.facilities.name like 'Tennis Court %' 
order by cd.bookings.starttime;

---3. How can you output a list of all members who have recommended another member? Ensure that there are no duplicates in the list, and that results are ordered by (surname, firstname).

select b.firstname as firstname , b.surname as surname 
from cd.members a join cd.members b on b.memid = a.recommendedby 
group by b.firstname, b.surname 
order by surname, firstname;

---4. How can you output a list of all members, including the individual who recommended them (if any)? Ensure that results are ordered by (surname, firstname).

select  a.firstname as memfname, a.surname as memsname, b.firstname as recfname , b.surname as recsname 
from cd.members a left join cd.members b on b.memid = a.recommendedby 
order by memsname, memfname;

---5. How can you produce a list of all members who have used a tennis court? Include in your output the name of the court, and the name of the member formatted as a single column. Ensure no duplicate data, and order by the member name.

select a.firstname ||' '|| a.surname as member,c.name as facility 
from cd.members a join cd.bookings b  on a.memid = b.memid join cd.facilities c on b.facid = c.facid 
where c.name like 'Tennis Court %' 
group by member, facility 
order by member;

6. 




---7. How can you output a list of all members, including the individual who recommended them (if any), without using any joins? Ensure that there are no duplicates in the list, and that each firstname + surname pairing is formatted as a column and ordered.

select a.firstname ||' '|| a.surname as member,
(select b.firstname ||' '|| b.surname as recommender 
from cd.members b where b.memid = a.recommendedby) 
from cd.members a 
group by member, recommender 
order by member;







