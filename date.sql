--- 1. Produce a timestamp for 1 a.m. on the 31st of August 2012.

select timestamp '2012-08-31 01:00:00';

--- 2. Find the result of subtracting the timestamp '2012-07-30 01:00:00' from the timestamp '2012-08-31 01:00:00'

select (timestamp '2012-08-31 01:00:00' - timestamp '2012-07-30 01:00:00') as interval;

--- 3. Get the day of the month from the timestamp '2012-08-31' as an integer.

select extract(day from timestamp '2012-08-31');

--- 4. Work out the number of seconds between the timestamps '2012-08-31 01:00:00' and '2012-09-02 00:00:00'

select extract(epoch from (timestamp '2012-09-02 00:00:00' - timestamp '2012-08-31 01:00:00'));

--- 5. Return a list of the start and end time of the last 10 bookings (ordered by the time at which they end, followed by the time at which they start) in the system.

select starttime, starttime + slots * ( interval '30 minutes') endtime
from cd.bookings order by endtime desc, starttime desc limit 10;

--- 6. Return a count of bookings for each month, sorted by month

select date_trunc('month', starttime) as m, count(*)
from cd.bookings
group by m
order by m;
