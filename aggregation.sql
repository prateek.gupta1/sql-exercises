--->1. For our first foray into aggregates, were going to stick to something simple. We want to know how many facilities exist - simply produce a total count.

select count(*) from cd.facilities;

--->2. Produce a count of the number of facilities that have a cost to guests of 10 or more.

select count(*) from cd.facilities where guestcost >= 10;

--- 3. Produce a count of the number of recommendations each member has made. Order by member ID.

select recommendedby, count(*) from cd.members where recommendedby is not null group by recommendedby order by recommendedby;

--- 4. Produce a list of the total number of slots booked per facility. For now, just produce an output table consisting of facility id and slots, sorted by facility id.

select facid, sum(slots) as Total_Slots from cd.bookings group by facid order by facid;

--- 5. Produce a list of the total number of slots booked per facility in the month of September 2012. Produce an output table consisting of facility id and slots, sorted by the number of slots.

select facid, sum(slots) as total_slots 
from cd.bookings where starttime >= '2012-09-01' and 
starttime < '2012-10-01' group by facid order by total_slots;

--- 6. Produce a list of the total number of slots booked per facility per month in the year of 2012. Produce an output table consisting of facility id and slots, sorted by the id and month.

select facid, extract(month from starttime) as month, 
sum(slots) as total_slots from cd.bookings 
where starttime >= '2012-01-01' and starttime < '2013-01-01' 
group by facid, month order by facid, month;

--- 7. Produce a list of facilities with more than 1000 slots booked. Produce an output table consisting of facility id and hours, sorted by facility id.

select facid, sum(slots) as total_slots 
from cd.bookings group by facid 
having sum(slots) > 1000 order by facid;

--- 8. Produce a list of facilities with a total revenue less than 1000. Produce an output table consisting of facility name and revenue, sorted by revenue. Remember that theres a different cost for guests and members!

select cd.facilities.name, sum(case 
				when memid=0 then slots * guestcost 
				else slots * membercost end) as revenue 
from cd.facilities join cd.bookings 
on cd.facilities.facid = cd.bookings.facid 
group by cd.facilities.name 
having sum(case 
	when memid=0 then slots * guestcost	  
	else slots * membercost end) < 1000;

--- 9.Produce a list of each member name, id, and their first booking after September 1st 2012. Order by member ID.

select m.surname, m.firstname, m.memid, min(b.starttime) as sttime 
from cd.members m join cd.bookings b on m.memid = b.memid 
where starttime > '2012-09-01' 
group by m.surname, m.firstname, m.memid 
order by m.memid;


--- 10. Produce a monotonically increasing numbered list of members, ordered by their date of joining. Remember that member IDs are not guaranteed to be sequential.

select row_number() over(order by joindate), firstname, surname from cd.members order by joindate;


--- 11. Produce a list of the top three revenue generating facilities (including ties). Output facility name and rank, sorted by rank and facility name.

 select name, rank from (
	select f.name as name, rank() over (order by sum(case
				when memid = 0 then slots * f.guestcost
				else slots * membercost
			end) desc) as rank
		from cd.bookings b
		inner join cd.facilities f
			on b.facid = f.facid
		group by f.name
	) as q1
	order by rank limit 3;

