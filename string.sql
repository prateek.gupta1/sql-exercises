--- 1.Output the names of all members, formatted as 'Surname, Firstname'

select surname ||', '|| firstname as name from cd.members;

--- 2. Find all facilities whose name begins with 'Tennis'. Retrieve all columns.

select * from cd.facilities where name like 'Tennis Court %';

---3. You've noticed that the club's member table has telephone numbers with very inconsistent formatting. You'd like to find all the telephone numbers that contain parentheses, returning the member ID and telephone number sorted by member ID.

select memid, telephone from cd.members where telephone ~ '[()]' 
order by memid;

---4. You'd like to produce a count of how many members you have whose surname starts with each letter of the alphabet. Sort by the letter, and don't worry about printing out a letter if the count is 0.

select left(surname , 1) as letter , count(*) from cd.members
group by letter order by letter;

---5. The telephone numbers in the database are very inconsistently formatted. You'd like to print a list of member ids and numbers that have had '-','(',')', and ' ' characters removed. Order by member id.

select memid, translate(telephone, '-() ', '') as t from cd.members
order by memid;
